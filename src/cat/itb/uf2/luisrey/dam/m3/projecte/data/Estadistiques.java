package cat.itb.uf2.luisrey.dam.m3.projecte.data;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class Estadistiques {
    //locales
    public static void local(Scanner scanner, League league) {
        System.out.println("introduce ID equipo");
        int id = scanner.nextInt();
        System.out.println("Partidos jugados");
        System.out.println(partidosJugados(league, id));
        System.out.println("Partidos ganados");
        partidosGanados(league, id);
        System.out.println("Partidos perdidos");
        partidosPerdidos(league, id);
    }

    private static void partidosPerdidos(League league, int id) {
        List<Match> perdidos = partidosJugados(league, id);
        int total = 0;
        Team team = League.searchById(id,league);
        for (Match lostMatch : perdidos){
            if (lostMatch.local.getName().equals(team.name)){
                if (lostMatch.getGolesL() < lostMatch.getGolesV())
                    total++;
            }else if (lostMatch.visitor.getName().equals(team.name)){
                if (lostMatch.getGolesL() > lostMatch.getGolesV())
                    total++;
            }
        }
        System.out.println(total);
    }

    private static void partidosGanados(League league, int id) {
        List<Match> ganados = partidosJugados(league, id);
        int total = 0;
        Team team = League.searchById(id,league);
        for (Match wonMatch : ganados){
            if (wonMatch.local.getName().equals(team.name)){
                if (wonMatch.getGolesL() > wonMatch.getGolesV())
                    total++;
            }else if (wonMatch.visitor.getName().equals(team.name)){
                if (wonMatch.getGolesL() < wonMatch.getGolesV())
                    total++;
            }
        }
        System.out.println(total);
    }

    private static List<Match> partidosJugados(League league, int id) {
        List<Match> playedMatches = new ArrayList<>();
        Team team = League.searchById(id,league);
        for (Match match: league.matches){
            if (match.local.getName().equals(team.name) || match.visitor.getName().equals(team.name)){
                playedMatches.add(match);
            }
        }
        return playedMatches;
    }


    //globales
    public static void global(Scanner scanner, League league) {
        System.out.println("Total partidos jugados");
        System.out.println(partidosTotales(league));
        System.out.println("Media partidos por equipo");
        avgMatches(league);
    }

    private static void avgMatches(League league) {
        int total = partidosTotales(league);
        System.out.println((double) total/league.teams.size());
    }
    

    private static int partidosTotales(League league) {
        int total = 0;
        for (int i = 0; i <league.matches.size(); i++) {
            total++;
        }
        return total;
    }


    public static void classificatoria(Scanner scanner, League league) {
        System.out.println("Classificacion por puntos");
        league.teams.sort(Comparator.comparing(Team::getPoints).thenComparing(Team::getId));
        for (Team team : league.teams){
            System.out.println(team + " - " + team.getPoints());
        }
    }
}
