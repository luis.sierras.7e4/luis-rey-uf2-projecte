package cat.itb.uf2.luisrey.dam.m3.projecte.data;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;


public class League {
    List<Team> teams = new ArrayList<>();
    public List<Match> matches = new ArrayList<>();


    // TEAMS
    //añadir equipo a la lista
    public void insertTeam(Team team) {
        teams.add(team);
    }

    //solicitar datos equipo y enviar a introducir
    public static void addTeam(Scanner scanner, League league) {
        Team newteam = readTeams(scanner);
        league.insertTeam(newteam);
    }

    //introducir datos de equipo
    public static Team readTeams(Scanner scanner) {
        System.out.println("Introduce el nombre del equipo: ");
        String nom = scanner.next();
        System.out.println("Ahora introduce el id del equipo: ");
        int id = scanner.nextInt();
        System.out.println("Por ultimo añade una abreviatura de este: ");
        String abr = scanner.next();
        while (abr.length() != 3) {
            System.out.println("abreviatura no valida, solo tres caracteres: ");
            abr = scanner.next();
        }

        return new Team(nom, id, abr);
    }

    //imprimir lista de equipos
    public static void printTeams(League league) {
        System.out.println("------------------------\n" +
                "--- Lista de Equipos ---\n" +
                "------------------------");

        for (Team team : league.teams) {
            System.out.println(team);
        }
    }

    //Modificar el equipo
    public static void ModifyTeam(Scanner scanner, League league) {
        System.out.println("Introduce el ID del equipo que deseas modificar");
        int id = scanner.nextInt();
        String name;
        for (Team team : league.teams) {
            if (id == team.id) {
                System.out.println("Introduce el nuevo nombre del equipo");
                name = scanner.next();
                team.setName(name);
                System.out.println("introduce nueva abreviación: ");
                String abr = scanner.next();
                team.setAbreviacion(abr);
            }
        }
    }

    public static Team searchById(int id, League league) {
        for (Team team : league.teams) {
            if (id == team.id) {
                return team;
            }
        }
        return null;
    }


    // MATCHES
    //añadir partidos a la lista
    public void insertMatch(Match match) {
        matches.add(match);
    }

    //solicitar datos partido y añadir a la lista
    public static void addMatch(Scanner scanner, League league) {
        Match newMatch = readMatch(scanner, league);
        league.insertMatch(newMatch);
    }

    //introducir datos de equipo
    public static Match readMatch(Scanner scanner, League league) {
        System.out.println("Introduce id partido: ");
        int idMatch = scanner.nextInt();

        System.out.println("ID equipo local: ");
        int idLocal = scanner.nextInt();
        Team localTeam = searchById(idLocal, league);
        System.out.println("Goles equipo local: ");
        int golesLocal = scanner.nextInt();

        System.out.println("ID equipo visitante: ");
        int idVisitor = scanner.nextInt();
        Team visitorTeam = searchById(idVisitor, league);
        System.out.println("Goles equipo visitante: ");
        int golesVisitor = scanner.nextInt();

        Team.puntuacion(golesLocal, golesVisitor, idLocal, idVisitor, league);
        return new Match(idMatch, localTeam, golesLocal, visitorTeam, golesVisitor);
    }

    //imprimir lista de partidas
    public static void printMatch(League league) {
        System.out.println("------------------------\n" +
                "--- Lista de Partidos ---\n" +
                "------------------------");
        for (Match match : league.matches) {
            System.out.println(match);
        }
    }

    //modificar partidos
    public static void ModifyMatchLocal(Scanner scanner, League league, Match match) {
        System.out.println("Introduce id nuevo equipo");
        int idlocal = scanner.nextInt();
        for (Team team : league.teams) {
            if (idlocal == team.id) {
                match.setLocal(team);
            }
        }
    }

    public static void ModifyMatchVisitor(Scanner scanner, League league, Match match) {
        System.out.println("Introduce id nuevo equipo");
        int idvisitor = scanner.nextInt();
        for (Team team : league.teams) {
            if (idvisitor == team.id) {
                match.setVisitor(team);
            }
        }
    }
}
