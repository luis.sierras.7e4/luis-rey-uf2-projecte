package cat.itb.uf2.luisrey.dam.m3.projecte.data;

public class Match {
    public int idMatch;
    Team local;
    int golesL;
    Team visitor;
    int golesV;


    public Match(int idMatch, Team local, int golesL, Team visitor, int golesV) {
        this.idMatch = idMatch;
        this.local = local;
        this.golesL = golesL;
        this.visitor = visitor;
        this.golesV = golesV;
    }

    //forma en que imprime partida

    public String toString() {
        return String.format("ID: %s\n" + "Local: %s (%d) - (%d) %s :Visitante", idMatch, local.getName(), getGolesL(), getGolesV(), visitor.getName());
    }

    //sets
    public void setLocal(Team local) {
        this.local = local;
    }

    public void setVisitor(Team visitor) {
        this.visitor = visitor;
    }

    //gets
    public int getGolesL() {
        return golesL;
    }

    public int getGolesV() {
        return golesV;
    }
}
