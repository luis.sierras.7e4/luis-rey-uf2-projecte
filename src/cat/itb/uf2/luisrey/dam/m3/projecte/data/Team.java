package cat.itb.uf2.luisrey.dam.m3.projecte.data;

public class Team {
    String name;
    int id;
    String Abreviacion;
    int points;

    public Team(String name, int id, String abreviacion) {
        this.name = name;
        this.id = id;
        this.Abreviacion = abreviacion;
    }

    //forma que imprime equipos
    public String toString() {
        return String.format(" %s - %s - (%s)", id, name, Abreviacion);
    }

    //gets
    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getAbr() {
        return Abreviacion;
    }

    public int getPoints() {
        return points;
    }


    //sets
    public void setName(String name) {
        this.name = name;
    }

    public void setAbreviacion(String abreviacion) {
        this.Abreviacion = abreviacion;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    public static void puntuacion(int golesLocal, int golesVisitor, int idLocal, int idVisitor, League league) {
        if (golesLocal > golesVisitor) {
            for (Team team : league.teams) {
                if (idLocal == team.id) {
                    int points = team.points + 3;
                    team.setPoints(points);
                }
            }
        } else if (golesLocal < golesVisitor) {
            for (Team team : league.teams) {
                if (idVisitor == team.id) {
                    int points = team.points + 3;
                    team.setPoints(points);
                }
            }
        } else {
            for (Team team : league.teams) {
                if (idLocal == team.id) {
                    int points = team.points + 1;
                    team.setPoints(points);
                }
            }
            for (Team team : league.teams) {
                if (idVisitor == team.id) {
                    int points = team.points + 1;
                    team.setPoints(points);
                }
            }
        }
    }
}