package cat.itb.uf2.luisrey.dam.m3.projecte.ui;

import cat.itb.uf2.luisrey.dam.m3.projecte.data.Estadistiques;
import cat.itb.uf2.luisrey.dam.m3.projecte.data.League;
import com.sun.tools.javac.Main;

import java.io.IOException;
import java.util.Scanner;

public class MenuEstadistiques {
    public static void displayEstadisticas(Scanner scanner, League league) {
        System.out.println("-------Estadisticas-------\n"+
                "1. Local\n"+
                "2. Global\n"+
                "3. Clasificación\n"+
                "0. Salir");

        int opcion = scanner.nextInt();
        switch (opcion) {
            case 1:
                Estadistiques.local(scanner,league);
                break;
            case 2:
                Estadistiques.global(scanner,league);
                break;
            case 3:
                Estadistiques.classificatoria(scanner, league);
                break;
            case 0:
                MainMenu.displayMenu(scanner, league);
                break;
        }
    }

}
